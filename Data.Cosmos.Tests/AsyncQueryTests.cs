﻿using FluentAssertions;
using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Xunit;

namespace Data.Cosmos.Tests
{
    public class AsyncQueryTests
    {
        /*public class CustomIdentityDbContext : DbContext
        {
            public CustomIdentityDbContext(DbContextOptions<CustomIdentityDbContext> options)
                :base(options)
            {

            }
            protected override void OnModelCreating(ModelBuilder modelBuilder)
            {
                base.OnModelCreating(modelBuilder);

                modelBuilder.Entity<IdentityUser>(b =>
                {
                });
            }
        }*/

        [Fact]
        public async Task TryReadAnExistingRoleAsync()
        {
            var b = new DbContextOptionsBuilder<CustomIdentityDbContext>();
            b.UseCosmos(
                "https://atlas-cosmos-db.documents.azure.com:443/",
                "6CsQiFiu9IyXGJGAxVk7puZ34FjbTNJJZhS3NcTU74D8EPTE0TbshEkMKqNWCbTfxzVDjxDOBFQBju6qoK5W9A==",
                databaseName: "pocidentitydb");
            
            using(var context = new CustomIdentityDbContext(b.Options, new OperationalStoreOptions()))
            {
                var user = await context.Query<IdentityUser>().FirstOrDefaultAsync(x => x.NormalizedUserName == "ADMIN");
                user.Should().NotBeNull();

                var role = await context.Query<IdentityRole>().FirstOrDefaultAsync(x => x.NormalizedName == "ADMINROLE");
                role.Should().NotBeNull();
            }
        }

        [Fact]
        public async Task TryReadAnExistingRoleSync()
        {
            var b = new DbContextOptionsBuilder<CustomIdentityDbContext>();
            b.UseCosmos(
                "https://atlas-cosmos-db.documents.azure.com:443/",
                "6CsQiFiu9IyXGJGAxVk7puZ34FjbTNJJZhS3NcTU74D8EPTE0TbshEkMKqNWCbTfxzVDjxDOBFQBju6qoK5W9A==",
                databaseName: "pocidentitydb");

            using (var context = new CustomIdentityDbContext(b.Options, new OperationalStoreOptions()))
            {
                var user = context.Query<IdentityUser>().FirstOrDefault(x => x.NormalizedUserName == "ADMIN");
                user.Should().NotBeNull();

                var role = context.Query<IdentityRole>().FirstOrDefault(x => x.NormalizedName == "ADMINROLE");
                role.Should().NotBeNull();
            }
        }
    }
}
