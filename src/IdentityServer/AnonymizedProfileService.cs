﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace IdentityServer
{
    public class AnonymizedProfileService : IProfileService
    {
        private readonly IUserSession _session;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly UserManager<IdentityUser> _userManager;

        public AnonymizedProfileService(IUserSession session, IHttpContextAccessor contextAccessor, UserManager<IdentityUser> userManager)
        {
            _session = session;
            _contextAccessor = contextAccessor;
            _userManager = userManager;
        }

        public Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var emailClaimName = "name";
            if (context.Subject.HasClaim(c => c.Type == emailClaimName))
            {
                context.IssuedClaims.Add(new Claim("customclaim", context.Subject.Claims.First(x => x.Type == emailClaimName).Value));
            }
            else
            {
                context.IssuedClaims.Add(new Claim("customclaim", "customclaimvalue@mail.com"));
            }
            context.IssuedClaims.AddRange(context.Subject.Claims/*.Take(2)*/);
            
            context.IssuedClaims.Add(new Claim("role", "AdminRole"));
            context.IssuedClaims.Add(new Claim("IsBackendUser", "true"));
            context.IssuedClaims.Add(new Claim("roles", "BackendUsers, Administrators"));
            context.IssuedClaims.Add(new Claim("role", "BackendUsers"));
            context.IssuedClaims.Add(new Claim("role", "Administrators"));
            return Task.CompletedTask;
        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            if (context.Subject.Identity.AuthenticationType == "Identity.Application")
            {
                var mfaProviders = new[]
                {
                    "local",
                    "google"
                };

                var providerClaimName = "idp";

                if (context.Subject.Identity is ClaimsIdentity ci && ci.HasClaim(x => x.Type == providerClaimName))
                {
                    var providerClaim = ci.Claims.First(x => x.Type == providerClaimName);

                    if (!mfaProviders.Contains(providerClaim.Value)) return;

                    var emailClaim = ci.Claims.First(x => x.Type == "email");

                    var user = await _userManager.FindByEmailAsync(emailClaim.Value);

                    if (user.TwoFactorEnabled) return;

                    //he needs two activate the two factor authentication
                    //via a normal flow
                    context.IsActive = false;
                }
                else
                {
                    throw new NotSupportedException();
                }
            }
        }
    }
}