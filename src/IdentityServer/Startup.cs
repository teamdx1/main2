﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using Data;
using IdentityServer4;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using IdentityServer4.EntityFramework.Options;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace IdentityServer
{
    public class Startup
    {
        public IHostingEnvironment Environment { get; }

        public Startup(IHostingEnvironment environment)
        {
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
            Environment = environment;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging(o =>
            {
                o.AddConsole()
                    .AddDebug()
                    .AddEventSourceLogger()
                    .SetMinimumLevel(LogLevel.Trace)
                    ;
            });

            services.AddSingleton<ICorsPolicyService>(x =>
            {
                return new DefaultCorsPolicyService(x.GetRequiredService<ILoggerFactory>().CreateLogger<DefaultCorsPolicyService>())
                {
                    AllowedOrigins = { "*" },
                    AllowAll = true
                };
            });

            services.AddCors();
            // uncomment, if you wan to add an MVC-based UI
            services.AddMvc(
                    o => o.EnableEndpointRouting = false
                    )
                .SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Version_3_0);

            services.AddDbContext<CustomIdentityDbContext>(ConfigureCosmosDb);
            services.AddDbContext<LogDbContext>(ConfigureCosmosDb);
            services.AddDbContext<AdminConfigurationDbContext>(ConfigureCosmosDb);

            services.AddIdentity<IdentityUser, IdentityRole>(o =>
                {
                    o.Password.RequireDigit = false;
                    o.Password.RequiredLength = 6;
                    o.SignIn.RequireConfirmedEmail = false;
                    o.SignIn.RequireConfirmedPhoneNumber = false;
                    
                    o.Lockout.AllowedForNewUsers = false;
                    o.Lockout.MaxFailedAccessAttempts = 1000;
                })
                .AddEntityFrameworkStores<CustomIdentityDbContext>()
                .AddDefaultTokenProviders();


            var builder = services.AddIdentityServer()
                /*
                .AddInMemoryIdentityResources(Config.GetIdentityResources())
                .AddInMemoryApiResources(Config.GetApis())
                .AddInMemoryClients(Config.GetClients())
                */

                //.AddTestUsers(Config.GetUsers())
                .AddAspNetIdentity<IdentityUser>()
                .AddProfileService<AnonymizedProfileService>()
                .AddConfigurationStore<AdminConfigurationDbContext>(options =>
                {
                    options.ConfigureDbContext = ConfigureCosmosDb;
                })
                // this adds the operational data from DB (codes, tokens, consents)
                .AddOperationalStore<CustomIdentityDbContext>(options =>
                {
                    options.ConfigureDbContext = ConfigureCosmosDb;

                    // this enables automatic token cleanup. this is optional.
                    options.EnableTokenCleanup = true;
                })
                //.AddWsFederation()
                ;
            ;

            if (true/*Environment.IsDevelopment()*/)
            {
                builder.AddDeveloperSigningCredential();
            }
            else
            {
                throw new Exception("need to configure key material");
            }

            services.AddAuthentication()
                //.AddGoogle("Google", options =>
                //{
                //    options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;

                //    options.ClientId = "1061792894115-58e0vknik0un3f7t17ii9afgvt5tm4eb.apps.googleusercontent.com";
                //    options.ClientSecret = "vvjThJuG82x3N2l85bSdIvHs";
                //})
                .AddOpenIdConnect("adfs", "AD authentication", options =>
                {
                    options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;
                    options.SignOutScheme = IdentityServerConstants.SignoutScheme;

                    options.Authority = "https://Sts.adfslab.ga/adfs";
                    options.ClientId = "db91eda1-919c-4560-963c-9c0e10e333b0";
                    options.ClientSecret = "AsQrLOFXwu57KHMszaflBoa3eYIxazW1vRQ2ysme";
                    options.ResponseType = "id_token";

                    options.CallbackPath = "/signin-adfs";
                    options.SignedOutCallbackPath = "/signout-callback-adfs";
                    //options.RemoteSignOutPath = "/signin-adfs";

                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        NameClaimType = "name",
                        RoleClaimType = "role"
                    };
                })
                .AddCookie("TwoFactor");
        }

        private static DbContextOptions<TContext> DbContextOptionsFactory<TContext>(
            IServiceProvider applicationServiceProvider,
            Action<IServiceProvider, DbContextOptionsBuilder> optionsAction)
            where TContext : DbContext
        {
            var builder = new DbContextOptionsBuilder<TContext>(
                new DbContextOptions<TContext>(new Dictionary<Type, IDbContextOptionsExtension>()));

            builder.UseApplicationServiceProvider(applicationServiceProvider);

            optionsAction?.Invoke(applicationServiceProvider, builder);

            return builder.Options;
        }

        private static void ConfigureCosmosDb(DbContextOptionsBuilder b)
        {
            b.UseCosmos(
                "https://identityserverdi.documents.azure.com:443/",
                "nOsB6FiJQAUJtcYcfSUSZCOfIZvMJcrDLXeMBy4pN37S4nVyfXshQiVlNcIhzINw3FxxfeqL9JDsy275DhOFYQ==",
                databaseName: "pocidentitydb",
                builder => { builder.Region("West Europe"); }
                );
        }

        public void Configure(IApplicationBuilder app)
        {
            //if (Environment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder =>
                builder
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowAnyOrigin()
                );

            // uncomment if you want to support static files
            app.UseStaticFiles();

            // Enables the application to temporarily store user information when they are verifying the second factor in the two-factor authentication process.
            app.UseIdentityServer();

            // uncomment, if you wan to add an MVC-based UI
            app.UseMvcWithDefaultRoute();
        }
    }
}