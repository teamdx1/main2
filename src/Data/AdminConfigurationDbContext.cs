﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Entities;
using IdentityServer4.EntityFramework.Extensions;
using IdentityServer4.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Skoruba.IdentityServer4.Admin.EntityFramework.DbContexts;
using Skoruba.IdentityServer4.Admin.EntityFramework.Interfaces;

namespace Data
{
    public class AdminConfigurationDbContext : DbContext,  IAdminConfigurationDbContext
    {
        private readonly ConfigurationStoreOptions storeOptions;

        public AdminConfigurationDbContext(DbContextOptions<AdminConfigurationDbContext> options, ConfigurationStoreOptions storeOptions) : base(options)
        {
            this.storeOptions = storeOptions;
        }

        public DbSet<ApiSecret> ApiSecrets { get; set; }
        public DbSet<ApiScope> ApiScopes { get; set; }
        public DbSet<ApiScopeClaim> ApiScopeClaims { get; set; }
        public DbSet<IdentityClaim> IdentityClaims { get; set; }
        public DbSet<ApiResourceClaim> ApiResourceClaims { get; set; }
        public DbSet<ClientGrantType> ClientGrantTypes { get; set; }
        public DbSet<ClientScope> ClientScopes { get; set; }
        public DbSet<ClientSecret> ClientSecrets { get; set; }
        public DbSet<ClientPostLogoutRedirectUri> ClientPostLogoutRedirectUris { get; set; }
        public DbSet<ClientCorsOrigin> ClientCorsOrigins { get; set; }
        public DbSet<ClientIdPRestriction> ClientIdPRestrictions { get; set; }
        public DbSet<ClientRedirectUri> ClientRedirectUris { get; set; }
        public DbSet<ClientClaim> ClientClaims { get; set; }
        public DbSet<ClientProperty> ClientProperties { get; set; }
        public DbSet<IdentityResourceProperty> IdentityResourceProperties { get; set; }
        public DbSet<ApiResourceProperty> ApiResourceProperties { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            if (!string.IsNullOrWhiteSpace(storeOptions.DefaultSchema))
                modelBuilder.HasDefaultSchema(storeOptions.DefaultSchema);
            modelBuilder.Entity<Client>((Action<EntityTypeBuilder<Client>>)(client =>
            {
                client.ToTable<Client>(storeOptions.Client.Name);
                client.HasKey((Expression<Func<Client, object>>)(x => (object)x.Id));
                client.Property<string>((Expression<Func<Client, string>>)(x => x.ClientId)).HasMaxLength(200).IsRequired(true);
                client.Property<string>((Expression<Func<Client, string>>)(x => x.ProtocolType)).HasMaxLength(200).IsRequired(true);
                client.Property<string>((Expression<Func<Client, string>>)(x => x.ClientName)).HasMaxLength(200);
                client.Property<string>((Expression<Func<Client, string>>)(x => x.ClientUri)).HasMaxLength(2000);
                client.Property<string>((Expression<Func<Client, string>>)(x => x.LogoUri)).HasMaxLength(2000);
                client.Property<string>((Expression<Func<Client, string>>)(x => x.Description)).HasMaxLength(1000);
                client.Property<string>((Expression<Func<Client, string>>)(x => x.FrontChannelLogoutUri)).HasMaxLength(2000);
                client.Property<string>((Expression<Func<Client, string>>)(x => x.BackChannelLogoutUri)).HasMaxLength(2000);
                client.Property<string>((Expression<Func<Client, string>>)(x => x.ClientClaimsPrefix)).HasMaxLength(200);
                client.Property<string>((Expression<Func<Client, string>>)(x => x.PairWiseSubjectSalt)).HasMaxLength(200);
                client.Property<string>((Expression<Func<Client, string>>)(x => x.UserCodeType)).HasMaxLength(100);
                //client.HasIndex((Expression<Func<Client, object>>)(x => x.ClientId)).IsUnique(true);
                client.HasMany<ClientGrantType>((Expression<Func<Client, IEnumerable<ClientGrantType>>>)(x => x.AllowedGrantTypes)).WithOne((Expression<Func<ClientGrantType, Client>>)(x => x.Client)).HasForeignKey((Expression<Func<ClientGrantType, object>>)(x => (object)x.ClientId)).IsRequired(true).OnDelete(DeleteBehavior.Cascade);
                client.HasMany<ClientRedirectUri>((Expression<Func<Client, IEnumerable<ClientRedirectUri>>>)(x => x.RedirectUris)).WithOne((Expression<Func<ClientRedirectUri, Client>>)(x => x.Client)).HasForeignKey((Expression<Func<ClientRedirectUri, object>>)(x => (object)x.ClientId)).IsRequired(true).OnDelete(DeleteBehavior.Cascade);
                client.HasMany<ClientPostLogoutRedirectUri>((Expression<Func<Client, IEnumerable<ClientPostLogoutRedirectUri>>>)(x => x.PostLogoutRedirectUris)).WithOne((Expression<Func<ClientPostLogoutRedirectUri, Client>>)(x => x.Client)).HasForeignKey((Expression<Func<ClientPostLogoutRedirectUri, object>>)(x => (object)x.ClientId)).IsRequired(true).OnDelete(DeleteBehavior.Cascade);
                client.HasMany<ClientScope>((Expression<Func<Client, IEnumerable<ClientScope>>>)(x => x.AllowedScopes)).WithOne((Expression<Func<ClientScope, Client>>)(x => x.Client)).HasForeignKey((Expression<Func<ClientScope, object>>)(x => (object)x.ClientId)).IsRequired(true).OnDelete(DeleteBehavior.Cascade);
                client.HasMany<ClientSecret>((Expression<Func<Client, IEnumerable<ClientSecret>>>)(x => x.ClientSecrets)).WithOne((Expression<Func<ClientSecret, Client>>)(x => x.Client)).HasForeignKey((Expression<Func<ClientSecret, object>>)(x => (object)x.ClientId)).IsRequired(true).OnDelete(DeleteBehavior.Cascade);
                client.HasMany<ClientClaim>((Expression<Func<Client, IEnumerable<ClientClaim>>>)(x => x.Claims)).WithOne((Expression<Func<ClientClaim, Client>>)(x => x.Client)).HasForeignKey((Expression<Func<ClientClaim, object>>)(x => (object)x.ClientId)).IsRequired(true).OnDelete(DeleteBehavior.Cascade);
                client.HasMany<ClientIdPRestriction>((Expression<Func<Client, IEnumerable<ClientIdPRestriction>>>)(x => x.IdentityProviderRestrictions)).WithOne((Expression<Func<ClientIdPRestriction, Client>>)(x => x.Client)).HasForeignKey((Expression<Func<ClientIdPRestriction, object>>)(x => (object)x.ClientId)).IsRequired(true).OnDelete(DeleteBehavior.Cascade);
                client.HasMany<ClientCorsOrigin>((Expression<Func<Client, IEnumerable<ClientCorsOrigin>>>)(x => x.AllowedCorsOrigins)).WithOne((Expression<Func<ClientCorsOrigin, Client>>)(x => x.Client)).HasForeignKey((Expression<Func<ClientCorsOrigin, object>>)(x => (object)x.ClientId)).IsRequired(true).OnDelete(DeleteBehavior.Cascade);
                client.HasMany<ClientProperty>((Expression<Func<Client, IEnumerable<ClientProperty>>>)(x => x.Properties)).WithOne((Expression<Func<ClientProperty, Client>>)(x => x.Client)).HasForeignKey((Expression<Func<ClientProperty, object>>)(x => (object)x.ClientId)).IsRequired(true).OnDelete(DeleteBehavior.Cascade);
            }));
            modelBuilder.Entity<ClientGrantType>((Action<EntityTypeBuilder<ClientGrantType>>)(grantType =>
            {
                grantType.ToTable<ClientGrantType>(storeOptions.ClientGrantType.Name);
                grantType.Property<string>((Expression<Func<ClientGrantType, string>>)(x => x.GrantType)).HasMaxLength(250).IsRequired(true);
            }));
            modelBuilder.Entity<ClientRedirectUri>((Action<EntityTypeBuilder<ClientRedirectUri>>)(redirectUri =>
            {
                redirectUri.ToTable<ClientRedirectUri>(storeOptions.ClientRedirectUri.Name);
                redirectUri.Property<string>((Expression<Func<ClientRedirectUri, string>>)(x => x.RedirectUri)).HasMaxLength(2000).IsRequired(true);
            }));
            modelBuilder.Entity<ClientPostLogoutRedirectUri>((Action<EntityTypeBuilder<ClientPostLogoutRedirectUri>>)(postLogoutRedirectUri =>
            {
                postLogoutRedirectUri.ToTable<ClientPostLogoutRedirectUri>(storeOptions.ClientPostLogoutRedirectUri.Name);
                postLogoutRedirectUri.Property<string>((Expression<Func<ClientPostLogoutRedirectUri, string>>)(x => x.PostLogoutRedirectUri)).HasMaxLength(2000).IsRequired(true);
            }));
            modelBuilder.Entity<ClientScope>((Action<EntityTypeBuilder<ClientScope>>)(scope =>
            {
                scope.ToTable<ClientScope>(storeOptions.ClientScopes.Name);
                scope.Property<string>((Expression<Func<ClientScope, string>>)(x => x.Scope)).HasMaxLength(200).IsRequired(true);
            }));
            modelBuilder.Entity<ClientSecret>((Action<EntityTypeBuilder<ClientSecret>>)(secret =>
            {
                secret.ToTable<ClientSecret>(storeOptions.ClientSecret.Name);
                secret.Property<string>((Expression<Func<ClientSecret, string>>)(x => x.Value)).HasMaxLength(4000).IsRequired(true);
                secret.Property<string>((Expression<Func<ClientSecret, string>>)(x => x.Type)).HasMaxLength(250).IsRequired(true);
                secret.Property<string>((Expression<Func<ClientSecret, string>>)(x => x.Description)).HasMaxLength(2000);
            }));
            modelBuilder.Entity<ClientClaim>((Action<EntityTypeBuilder<ClientClaim>>)(claim =>
            {
                claim.ToTable<ClientClaim>(storeOptions.ClientClaim.Name);
                claim.Property<string>((Expression<Func<ClientClaim, string>>)(x => x.Type)).HasMaxLength(250).IsRequired(true);
                claim.Property<string>((Expression<Func<ClientClaim, string>>)(x => x.Value)).HasMaxLength(250).IsRequired(true);
            }));
            modelBuilder.Entity<ClientIdPRestriction>((Action<EntityTypeBuilder<ClientIdPRestriction>>)(idPRestriction =>
            {
                idPRestriction.ToTable<ClientIdPRestriction>(storeOptions.ClientIdPRestriction.Name);
                idPRestriction.Property<string>((Expression<Func<ClientIdPRestriction, string>>)(x => x.Provider)).HasMaxLength(200).IsRequired(true);
            }));
            modelBuilder.Entity<ClientCorsOrigin>((Action<EntityTypeBuilder<ClientCorsOrigin>>)(corsOrigin =>
            {
                corsOrigin.ToTable<ClientCorsOrigin>(storeOptions.ClientCorsOrigin.Name);
                corsOrigin.Property<string>((Expression<Func<ClientCorsOrigin, string>>)(x => x.Origin)).HasMaxLength(150).IsRequired(true);
            }));
            modelBuilder.Entity<ClientProperty>((Action<EntityTypeBuilder<ClientProperty>>)(property =>
            {
                property.ToTable<ClientProperty>(storeOptions.ClientProperty.Name);
                property.Property<string>((Expression<Func<ClientProperty, string>>)(x => x.Key)).HasMaxLength(250).IsRequired(true);
                property.Property<string>((Expression<Func<ClientProperty, string>>)(x => x.Value)).HasMaxLength(2000).IsRequired(true);
            }));

            ConfigureResourcesContext(modelBuilder, this.storeOptions);
        }

        public static void ConfigureResourcesContext(
      ModelBuilder modelBuilder,
      ConfigurationStoreOptions storeOptions)
        {
            if (!string.IsNullOrWhiteSpace(storeOptions.DefaultSchema))
                modelBuilder.HasDefaultSchema(storeOptions.DefaultSchema);
            modelBuilder.Entity<IdentityResource>((Action<EntityTypeBuilder<IdentityResource>>)(identityResource =>
            {
                identityResource.ToTable<IdentityResource>(storeOptions.IdentityResource.Name).HasKey((Expression<Func<IdentityResource, object>>)(x => (object)x.Id));
                identityResource.Property<string>((Expression<Func<IdentityResource, string>>)(x => x.Name)).HasMaxLength(200).IsRequired(true);
                identityResource.Property<string>((Expression<Func<IdentityResource, string>>)(x => x.DisplayName)).HasMaxLength(200);
                identityResource.Property<string>((Expression<Func<IdentityResource, string>>)(x => x.Description)).HasMaxLength(1000);
                identityResource.HasIndex((Expression<Func<IdentityResource, object>>)(x => x.Name)).IsUnique(true);
                identityResource.HasMany<IdentityClaim>((Expression<Func<IdentityResource, IEnumerable<IdentityClaim>>>)(x => x.UserClaims)).WithOne((Expression<Func<IdentityClaim, IdentityResource>>)(x => x.IdentityResource)).HasForeignKey((Expression<Func<IdentityClaim, object>>)(x => (object)x.IdentityResourceId)).IsRequired(true).OnDelete(DeleteBehavior.Cascade);
                identityResource.HasMany<IdentityResourceProperty>((Expression<Func<IdentityResource, IEnumerable<IdentityResourceProperty>>>)(x => x.Properties)).WithOne((Expression<Func<IdentityResourceProperty, IdentityResource>>)(x => x.IdentityResource)).HasForeignKey((Expression<Func<IdentityResourceProperty, object>>)(x => (object)x.IdentityResourceId)).IsRequired(true).OnDelete(DeleteBehavior.Cascade);
            }));
            modelBuilder.Entity<IdentityClaim>((Action<EntityTypeBuilder<IdentityClaim>>)(claim =>
            {
                claim.ToTable<IdentityClaim>(storeOptions.IdentityClaim.Name).HasKey((Expression<Func<IdentityClaim, object>>)(x => (object)x.Id));
                claim.Property<string>((Expression<Func<IdentityClaim, string>>)(x => x.Type)).HasMaxLength(200).IsRequired(true);
            }));
            modelBuilder.Entity<IdentityResourceProperty>((Action<EntityTypeBuilder<IdentityResourceProperty>>)(property =>
            {
                property.ToTable<IdentityResourceProperty>(storeOptions.IdentityResourceProperty.Name);
                property.Property<string>((Expression<Func<IdentityResourceProperty, string>>)(x => x.Key)).HasMaxLength(250).IsRequired(true);
                property.Property<string>((Expression<Func<IdentityResourceProperty, string>>)(x => x.Value)).HasMaxLength(2000).IsRequired(true);
            }));
            modelBuilder.Entity<ApiResource>((Action<EntityTypeBuilder<ApiResource>>)(apiResource =>
            {
                apiResource.ToTable<ApiResource>(storeOptions.ApiResource.Name).HasKey((Expression<Func<ApiResource, object>>)(x => (object)x.Id));
                apiResource.Property<string>((Expression<Func<ApiResource, string>>)(x => x.Name)).HasMaxLength(200).IsRequired(true);
                apiResource.Property<string>((Expression<Func<ApiResource, string>>)(x => x.DisplayName)).HasMaxLength(200);
                apiResource.Property<string>((Expression<Func<ApiResource, string>>)(x => x.Description)).HasMaxLength(1000);
                apiResource.HasIndex((Expression<Func<ApiResource, object>>)(x => x.Name)).IsUnique(true);
                apiResource.HasMany<ApiSecret>((Expression<Func<ApiResource, IEnumerable<ApiSecret>>>)(x => x.Secrets)).WithOne((Expression<Func<ApiSecret, ApiResource>>)(x => x.ApiResource)).HasForeignKey((Expression<Func<ApiSecret, object>>)(x => (object)x.ApiResourceId)).IsRequired(true).OnDelete(DeleteBehavior.Cascade);
                apiResource.HasMany<ApiScope>((Expression<Func<ApiResource, IEnumerable<ApiScope>>>)(x => x.Scopes)).WithOne((Expression<Func<ApiScope, ApiResource>>)(x => x.ApiResource)).HasForeignKey((Expression<Func<ApiScope, object>>)(x => (object)x.ApiResourceId)).IsRequired(true).OnDelete(DeleteBehavior.Cascade);
                apiResource.HasMany<ApiResourceClaim>((Expression<Func<ApiResource, IEnumerable<ApiResourceClaim>>>)(x => x.UserClaims)).WithOne((Expression<Func<ApiResourceClaim, ApiResource>>)(x => x.ApiResource)).HasForeignKey((Expression<Func<ApiResourceClaim, object>>)(x => (object)x.ApiResourceId)).IsRequired(true).OnDelete(DeleteBehavior.Cascade);
                apiResource.HasMany<ApiResourceProperty>((Expression<Func<ApiResource, IEnumerable<ApiResourceProperty>>>)(x => x.Properties)).WithOne((Expression<Func<ApiResourceProperty, ApiResource>>)(x => x.ApiResource)).HasForeignKey((Expression<Func<ApiResourceProperty, object>>)(x => (object)x.ApiResourceId)).IsRequired(true).OnDelete(DeleteBehavior.Cascade);
            }));
            modelBuilder.Entity<ApiSecret>((Action<EntityTypeBuilder<ApiSecret>>)(apiSecret =>
            {
                apiSecret.ToTable<ApiSecret>(storeOptions.ApiSecret.Name).HasKey((Expression<Func<ApiSecret, object>>)(x => (object)x.Id));
                apiSecret.Property<string>((Expression<Func<ApiSecret, string>>)(x => x.Description)).HasMaxLength(1000);
                apiSecret.Property<string>((Expression<Func<ApiSecret, string>>)(x => x.Value)).HasMaxLength(4000).IsRequired(true);
                apiSecret.Property<string>((Expression<Func<ApiSecret, string>>)(x => x.Type)).HasMaxLength(250).IsRequired(true);
            }));
            modelBuilder.Entity<ApiResourceClaim>((Action<EntityTypeBuilder<ApiResourceClaim>>)(apiClaim =>
            {
                apiClaim.ToTable<ApiResourceClaim>(storeOptions.ApiClaim.Name).HasKey((Expression<Func<ApiResourceClaim, object>>)(x => (object)x.Id));
                apiClaim.Property<string>((Expression<Func<ApiResourceClaim, string>>)(x => x.Type)).HasMaxLength(200).IsRequired(true);
            }));
            modelBuilder.Entity<ApiScope>((Action<EntityTypeBuilder<ApiScope>>)(apiScope =>
            {
                apiScope.ToTable<ApiScope>(storeOptions.ApiScope.Name).HasKey((Expression<Func<ApiScope, object>>)(x => (object)x.Id));
                apiScope.Property<string>((Expression<Func<ApiScope, string>>)(x => x.Name)).HasMaxLength(200).IsRequired(true);
                apiScope.Property<string>((Expression<Func<ApiScope, string>>)(x => x.DisplayName)).HasMaxLength(200);
                apiScope.Property<string>((Expression<Func<ApiScope, string>>)(x => x.Description)).HasMaxLength(1000);
                apiScope.HasIndex((Expression<Func<ApiScope, object>>)(x => x.Name)).IsUnique(true);
                apiScope.HasMany<ApiScopeClaim>((Expression<Func<ApiScope, IEnumerable<ApiScopeClaim>>>)(x => x.UserClaims)).WithOne((Expression<Func<ApiScopeClaim, ApiScope>>)(x => x.ApiScope)).HasForeignKey((Expression<Func<ApiScopeClaim, object>>)(x => (object)x.ApiScopeId)).IsRequired(true).OnDelete(DeleteBehavior.Cascade);
            }));
            modelBuilder.Entity<ApiScopeClaim>((Action<EntityTypeBuilder<ApiScopeClaim>>)(apiScopeClaim =>
            {
                apiScopeClaim.ToTable<ApiScopeClaim>(storeOptions.ApiScopeClaim.Name).HasKey((Expression<Func<ApiScopeClaim, object>>)(x => (object)x.Id));
                apiScopeClaim.Property<string>((Expression<Func<ApiScopeClaim, string>>)(x => x.Type)).HasMaxLength(200).IsRequired(true);
            }));
            modelBuilder.Entity<ApiResourceProperty>((Action<EntityTypeBuilder<ApiResourceProperty>>)(property =>
            {
                property.ToTable<ApiResourceProperty>(storeOptions.ApiResourceProperty.Name);
                property.Property<string>((Expression<Func<ApiResourceProperty, string>>)(x => x.Key)).HasMaxLength(250).IsRequired(true);
                property.Property<string>((Expression<Func<ApiResourceProperty, string>>)(x => x.Value)).HasMaxLength(2000).IsRequired(true);
            }));
        }

        public Task<int> SaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<IdentityResource> IdentityResources { get; set; }
        public DbSet<ApiResource> ApiResources { get; set; }
    }
}