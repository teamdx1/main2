﻿using AdminUI.Admin.Configuration.Interfaces;

namespace AdminUI.Admin.Configuration
{
    public class AdminConfiguration : IAdminConfiguration
    {
        public string IdentityAdminBaseUrl { get; set; } = "https://localhost:9000";

        public string IdentityAdminRedirectUri { get; set; } = "https://localhost:9000/signin-oidc";

        public string IdentityServerBaseUrl { get; set; } = "https://localhost:5000";
    }
}
