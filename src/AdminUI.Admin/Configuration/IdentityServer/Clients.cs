﻿using System.Collections.Generic;
using System.Security.Claims;
using IdentityServer4;
using IdentityServer4.Models;
using AdminUI.Admin.Configuration.Constants;
using AdminUI.Admin.Configuration.Interfaces;
using static IdentityServer4.IdentityServerConstants;

namespace AdminUI.Admin.Configuration.IdentityServer

{
    public class Clients
    {

        public static IEnumerable<Client> GetClients(IAdminConfiguration adminConfiguration)
        {

            return new List<Client>
            {

	            ///////////////////////////////////////////
	            // AdminUI.Admin Client
	            //////////////////////////////////////////
	            new Client
                {

                    ClientId = AuthenticationConsts.OidcClientId,
                    ClientName = AuthenticationConsts.OidcClientId,
                    ClientUri = adminConfiguration.IdentityAdminBaseUrl,

                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,

                    RedirectUris =
                    {
                        $"{adminConfiguration.IdentityAdminBaseUrl}/signin-oidc"
                    },
                    FrontChannelLogoutUri = $"{adminConfiguration.IdentityAdminBaseUrl}/signout-oidc",
                    PostLogoutRedirectUris = { $"{adminConfiguration.IdentityAdminBaseUrl}/signout-callback-oidc"},
                    AllowedCorsOrigins = { adminConfiguration.IdentityAdminBaseUrl },
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        "roles"
                    }
                },

                new Client
                {
                    ClientId = "client",

                    // no interactive user, use the clientid/secret for authentication
                    AllowedGrantTypes = GrantTypes.ClientCredentials,

                    // secret for authentication
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },

                    // scopes that client has access to
                    AllowedScopes = { "api1" }
                },
                // resource owner password grant client
                new Client
                {
                    ClientId = "ro.client",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,

                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = { "api1" }
                },
                // OpenID Connect implicit flow client (MVC)
                new Client
                {
                    ClientId = "mvc",
                    ClientName = "MVC Client",
                    AllowedGrantTypes = GrantTypes.Implicit,

                    // where to redirect to after login
                    RedirectUris = {
                        "https://atlastestclient1.azurewebsites.net/signin-oidc",
                        "https://sfandidsrvditest.azurewebsites.net/Sitefinity/Authenticate/OpenID/signin-custom"
                    },

                    // where to redirect to after logout
                    PostLogoutRedirectUris = {
                        "https://atlastestclient1.azurewebsites.net/signout-callback-oidc",
                        "https://sfandidsrvditest.azurewebsites.net/Sitefinity/Authenticate/OpenID/signout-custom"
                    },

                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        "groups",

                    }
                },
                new Client
                {
                    ClientId = "mvclocal",
                    ClientName = "MVC Client local",
                    AllowedGrantTypes = GrantTypes.Implicit,

                    // where to redirect to after login
                    RedirectUris = { "https://localhost:5002/signin-oidc" },

                    // where to redirect to after logout
                    PostLogoutRedirectUris = { "https://localhost:5002/signout-callback-oidc" },

                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile
                    }
                },

                new Client
                {
                    // realm identifier
                    ClientId = "urn:owinrp",
            
                    // must be set to WS-Federation
                     ProtocolType = ProtocolTypes.WsFederation,
                    ClientName = "Sitefinity",
                    AllowedGrantTypes = GrantTypes.Implicit,

                    // where to redirect to after login
                    RedirectUris = {
                        "https://atlastestclient1.azurewebsites.net/signin-oidc",
                        "https://sfandidsrvditest.azurewebsites.net/Sitefinity/Authenticate/OpenID/signin-custom"
                    },

                    // where to redirect to after logout
                    LogoUri = "https://sfandidsrvditest.azurewebsites.net/Sitefinity/Authenticate/OpenID/signout-custom",

                    IdentityTokenLifetime = 36000,

                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        "groups",
                    }
                },
            };

        }
    }
}