﻿using System;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4.EntityFramework.Mappers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using AdminUI.Admin.Configuration.Constants;
using AdminUI.Admin.Configuration.Identity;
using AdminUI.Admin.Configuration.IdentityServer;
using AdminUI.Admin.Configuration.Interfaces;
using Data;

namespace AdminUI.Admin.Helpers
{
    public static class DbMigrationHelpers
    {
        /// <summary>
        /// Generate migrations before running this method, you can use command bellow:
        /// Nuget package manager: Add-Migration DbInit -context AdminDbContext -output Data/Migrations
        /// Dotnet CLI: dotnet ef migrations add DbInit -c AdminDbContext -o Data/Migrations
        /// </summary>
        /// <param name="host"></param>
        public static async Task EnsureSeedData(IWebHost host)
        {
            using (var serviceScope = host.Services.CreateScope())
            {
                var services = serviceScope.ServiceProvider;

                await EnsureSeedData(services);
            }
        }

        private static async Task EnsureSeedData(IServiceProvider serviceProvider)
        {
            using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<AdminConfigurationDbContext>();
                var userManager = scope.ServiceProvider.GetRequiredService<UserManager<IdentityUser>>();
                var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

                var rootConfiguration = scope.ServiceProvider.GetRequiredService<IRootConfiguration>();

                var identityDb = scope.ServiceProvider.GetRequiredService<CustomIdentityDbContext>();
                identityDb.Database.EnsureCreated();

                scope.ServiceProvider.GetRequiredService<LogDbContext>().Database.EnsureCreated();

                context.Database.EnsureCreated();

                await EnsureSeedIdentityServerData(context, rootConfiguration.AdminConfiguration);
                await EnsureSeedIdentityData(userManager, roleManager);
            }
        }

        /// <summary>
        /// Generate default admin user / role
        /// </summary>
        private static async Task EnsureSeedIdentityData(UserManager<IdentityUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            // Create admin role
            if (!await roleManager.RoleExistsAsync(AuthorizationConsts.AdministrationRole))
            {
                var role = new IdentityRole { Name = AuthorizationConsts.AdministrationRole };

                await roleManager.CreateAsync(role);
            }

            // Create admin user
            if (await userManager.FindByNameAsync(Users.AdminUserName) != null) return;

            var user = new IdentityUser
            {
                UserName = Users.AdminUserName,
                Email = Users.AdminEmail,
                EmailConfirmed = true
            };

            var result = await userManager.CreateAsync(user, Users.AdminPassword);

            if (result.Succeeded)
            {
                await userManager.AddToRoleAsync(user, AuthorizationConsts.AdministrationRole);
            }
        }

        /// <summary>
        /// Generate default clients, identity and api resources
        /// </summary>
        private static async Task EnsureSeedIdentityServerData(AdminConfigurationDbContext context, IAdminConfiguration adminConfiguration)
        {
            var itemId = 100;
            if (!context.Clients.Any())
            {
                foreach (var client in Clients.GetClients(adminConfiguration).ToList())
                {
                    var item = client.ToEntity();
                    item.Id = itemId++;

                    foreach (var corsOrigin in item.AllowedCorsOrigins)
                    {
                        corsOrigin.Id = itemId++;
                    }

                    foreach (var corsOrigin in item.AllowedGrantTypes)
                    {
                        corsOrigin.Id = itemId++;
                    }

                    foreach (var corsOrigin in item.AllowedScopes)
                    {
                        corsOrigin.Id = itemId++;
                    }

                    foreach (var corsOrigin in item.PostLogoutRedirectUris)
                    {
                        corsOrigin.Id = itemId++;
                    }

                    foreach (var corsOrigin in item.RedirectUris)
                    {
                        corsOrigin.Id = itemId++;
                    }

                    foreach (var corsOrigin in item.ClientSecrets)
                    {
                        corsOrigin.Id = itemId++;
                    }

                    await context.Clients.AddAsync(item);
                }

                await context.SaveChangesAsync();
            }

            itemId = 1000;

            if (!context.IdentityResources.Any())
            {
                var identityResources = ClientResources.GetIdentityResources().ToList();

                foreach (var resource in identityResources)
                {
                    var item = resource.ToEntity();
                    item.Id = itemId++;

                    foreach (var claim in item.UserClaims)
                    {
                        claim.Id = itemId++;
                    }

                    await context.IdentityResources.AddAsync(item);
                }

                await context.SaveChangesAsync();
            }

            itemId = 10000;

            if (!context.ApiResources.Any())
            {
                foreach (var resource in ClientResources.GetApiResources().ToList())
                {
                    var item = resource.ToEntity();
                    item.Id = itemId++;


                    foreach (var scope in item.Scopes)
                    {
                        scope.Id = itemId++;
                    }

                    foreach (var scope in item.Secrets)
                    {
                        scope.Id = itemId++;
                    }

                    foreach (var scope in item.UserClaims)
                    {
                        scope.Id = itemId++;
                    }

                    await context.ApiResources.AddAsync(item);
                }

                await context.SaveChangesAsync();
            }
        }
    }
}
