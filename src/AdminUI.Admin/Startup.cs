﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Skoruba.IdentityServer4.Admin.BusinessLogic.Extensions;
using Skoruba.IdentityServer4.Admin.BusinessLogic.Identity.Dtos.Identity;
using Skoruba.IdentityServer4.Admin.BusinessLogic.Identity.Extensions;
using AdminUI.Admin.Configuration.Interfaces;
using Skoruba.IdentityServer4.Admin.EntityFramework.DbContexts;
using AdminUI.Admin.Helpers;
using Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using IdentityServer4.EntityFramework.Storage;

namespace AdminUI.Admin
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var builder = new ConfigurationBuilder()
                    .SetBasePath(env.ContentRootPath)
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                    .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
                    .AddEnvironmentVariables();

            if (env.IsDevelopment())
            {
                builder.AddUserSecrets<Startup>();
            }

            Configuration = builder.Build();

            HostingEnvironment = env;
        }

        public IConfigurationRoot Configuration { get; }

        public IHostingEnvironment HostingEnvironment { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging(o =>
            {
                o.AddConsole()
                    .AddDebug()
                    .AddEventSourceLogger()
                    .SetMinimumLevel(LogLevel.Trace)
                    ;
            });
                
            services.ConfigureRootConfiguration(Configuration);
            var rootConfiguration = services.BuildServiceProvider().GetService<IRootConfiguration>();

            services.AddDbContext<CustomIdentityDbContext>(ConfigureCosmosDb);
            services.AddDbContext<LogDbContext>(ConfigureCosmosDb);

            services.AddAdminServices<AdminConfigurationDbContext, CustomIdentityDbContext, LogDbContext>();

            services.AddAuthenticationServices<CustomIdentityDbContext, IdentityUser, IdentityRole>(HostingEnvironment, rootConfiguration.AdminConfiguration);

            services.AddConfigurationDbContext<AdminConfigurationDbContext>(options => { options.ConfigureDbContext = ConfigureCosmosDb; });
            services.AddOperationalDbContext<CustomIdentityDbContext>(options => { options.ConfigureDbContext = ConfigureCosmosDb; });

            services.AddCors();
            // uncomment, if you wan to add an MVC-based UI
            services.AddMvc(
                    o => o.EnableEndpointRouting = false
                )
                .SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Version_3_0);
            services.AddMvcExceptionFilters();

            services.AddAdminAspNetIdentityServices<CustomIdentityDbContext, UserDto<string>, string, RoleDto<string>, string, string, string,
                                IdentityUser, IdentityRole, string, IdentityUserClaim<string>, IdentityUserRole<string>,
                                IdentityUserLogin<string>, IdentityRoleClaim<string>, IdentityUserToken<string>>();

            services.AddMvcLocalization();
            services.AddAuthorizationPolicies();
        }

        private static void ConfigureCosmosDb(DbContextOptionsBuilder b)
        {
            b.UseCosmos(
                "https://identityserverdi.documents.azure.com:443/",
                "nOsB6FiJQAUJtcYcfSUSZCOfIZvMJcrDLXeMBy4pN37S4nVyfXshQiVlNcIhzINw3FxxfeqL9JDsy275DhOFYQ==",
                databaseName: "pocidentitydb",
                o => { o.Region("West Europe"); }
            );
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.AddLogging(loggerFactory, Configuration);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseSecurityHeaders();
            app.UseStaticFiles();
            app.ConfigureAuthentificationServices(env);
            app.ConfigureLocalization();

            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}