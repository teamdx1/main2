﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using AdminUI.Admin.Helpers;

namespace AdminUI.Admin
{
    public class Program
    {
        private const string SeedArgs = "/seed";

        public static async Task Main(string[] args)
        {
            var seed = args.Any(x => x == SeedArgs);
            if (seed) args = args.Except(new[] { SeedArgs }).ToArray();

            var host = BuildWebHost(args);

            // Uncomment this to seed upon startup, alternatively pass in `dotnet run /seed` to seed using CLI
            // await DbMigrationHelpers.EnsureSeedData(host);
            if (true/*seed*/)
            {
                try
                {
                    await DbMigrationHelpers.EnsureSeedData(host);
                }
                catch(System.Exception ex)
                {
                    throw;
                }
            }

            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseApplicationInsights()
                   // .UseKestrel(c => c.AddServerHeader = false)
                   .UseStartup<Startup>()
                   //.UseSerilog()
                   .Build();
    }
}