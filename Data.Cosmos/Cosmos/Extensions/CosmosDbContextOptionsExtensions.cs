﻿// Copyright (c) .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore.Cosmos.Infrastructure;
using Microsoft.EntityFrameworkCore.Cosmos.Infrastructure.Internal;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Utilities;

namespace Microsoft.EntityFrameworkCore
{
    public static class CosmosDbContextOptionsExtensions
    {
        public static Type TryGetSequenceType(this Type type)
        {
            Type elementType = type.TryGetElementType(typeof(IEnumerable<>));
            if ((object)elementType != null)
                return elementType;
            return type.TryGetElementType(typeof(IAsyncEnumerable<>));
        }

        public static Type TryGetElementType(this Type type, Type interfaceOrBaseType)
        {
            if (type.GetTypeInfo().IsGenericTypeDefinition)
                return (Type)null;
            IEnumerable<Type> typeImplementations = SharedTypeExtensions.GetGenericTypeImplementations(type, interfaceOrBaseType);
            Type type1 = (Type)null;
            foreach (Type type2 in typeImplementations)
            {
                if (type1 == (Type)null)
                {
                    type1 = type2;
                }
                else
                {
                    type1 = (Type)null;
                    break;
                }
            }
            if ((object)type1 == null)
                return (Type)null;
            return ((IEnumerable<Type>)type1.GetTypeInfo().GenericTypeArguments).FirstOrDefault<Type>();
        }

        public static DbContextOptionsBuilder<TContext> UseCosmos<TContext>(
              this DbContextOptionsBuilder<TContext> optionsBuilder,
              string serviceEndPoint,
              string authKeyOrResourceToken,
              string databaseName,
              Action<CosmosDbContextOptionsBuilder> cosmosOptionsAction = null)
            where TContext : DbContext
            => (DbContextOptionsBuilder<TContext>)UseCosmos(
                (DbContextOptionsBuilder)optionsBuilder,
                serviceEndPoint,
                authKeyOrResourceToken,
                databaseName,
                cosmosOptionsAction);

        public static DbContextOptionsBuilder UseCosmos(
              this DbContextOptionsBuilder optionsBuilder,
              string serviceEndPoint,
              string authKeyOrResourceToken,
              string databaseName,
              Action<CosmosDbContextOptionsBuilder> cosmosOptionsAction = null)
        {
            Check.NotNull(optionsBuilder, nameof(optionsBuilder));
            Check.NotNull(serviceEndPoint, nameof(serviceEndPoint));
            Check.NotEmpty(authKeyOrResourceToken, nameof(authKeyOrResourceToken));
            Check.NotEmpty(databaseName, nameof(databaseName));

            var extension = optionsBuilder.Options.FindExtension<CosmosDbOptionsExtension>()
                            ?? new CosmosDbOptionsExtension();

            extension = extension
                .WithServiceEndPoint(serviceEndPoint)
                .WithAuthKeyOrResourceToken(authKeyOrResourceToken)
                .WithDatabaseName(databaseName);

            ((IDbContextOptionsBuilderInfrastructure)optionsBuilder).AddOrUpdateExtension(extension);

            cosmosOptionsAction?.Invoke(new CosmosDbContextOptionsBuilder(optionsBuilder));

            return optionsBuilder;
        }

        public static DbContextOptionsBuilder UseCosmos(
              this DbContextOptionsBuilder optionsBuilder,
              string serviceEndPoint,
              string authKeyOrResourceToken,
              string databaseName,
              string regionName,
              Action<CosmosDbContextOptionsBuilder> cosmosOptionsAction = null)
        {
            Check.NotNull(optionsBuilder, nameof(optionsBuilder));
            Check.NotNull(serviceEndPoint, nameof(serviceEndPoint));
            Check.NotEmpty(authKeyOrResourceToken, nameof(authKeyOrResourceToken));
            Check.NotEmpty(databaseName, nameof(databaseName));
            Check.NotEmpty(regionName, nameof(regionName));

            var extension = optionsBuilder.Options.FindExtension<CosmosDbOptionsExtension>()
                            ?? new CosmosDbOptionsExtension();

            extension = extension
                .WithServiceEndPoint(serviceEndPoint)
                .WithAuthKeyOrResourceToken(authKeyOrResourceToken)
                .WithDatabaseName(databaseName)
                .WithCurrentRegion(regionName)
                ;

            ((IDbContextOptionsBuilderInfrastructure)optionsBuilder).AddOrUpdateExtension(extension);

            cosmosOptionsAction?.Invoke(new CosmosDbContextOptionsBuilder(optionsBuilder));

            return optionsBuilder;
        }
    }
}
